import { Injectable } from '@angular/core';
import { User } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private users: User[] = [
    {
      username: 'hasan',
      password: 'hasan'
    }
  ];
  constructor() { }

  getUser(username: String){
    return {...this.users.find(user => {
      return user.username === username;
    })
  };
  }
}
