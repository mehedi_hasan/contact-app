import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { User } from '../user.model';
import { Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {
  user: User;
  public username: string;
  public password: string;
  public errorMsg: string;
  toast: any;
  constructor(
      private usersService: UsersService,
      private router: Router,
      private alertCtrl: AlertController,
      private toastCtrl: ToastController,
    ) { }
  ngOnInit() {}
  login() {
    this.user = this.usersService.getUser(this.username);
    if (this.user.username === this.username && this.user.password === this.password) {
      this.showToast('Successfully Logged In');
      this.router.navigateByUrl('/contact-list');
    } else {
      this.showAlert('Username or Password is not matched!');
    }
  }
  async showAlert(msg: any) {
    const alert = await this.alertCtrl.create({
      message: msg,
      buttons: [
        {
          text: 'ok',
          cssClass: 'icon-color',
          handler: () => {
          }
        },
      ]
    });
    await alert.present();
  }
  showToast(msg: any) {
    this.toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    }).then((toastData) => {
      toastData.present();
    });
  }
}
