import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'welcome', pathMatch: 'full' },
  { path: 'welcome', loadChildren: './welcome/welcome.module#WelcomePageModule' },
  { path: 'contact-list',
    children: [
      { path: '', loadChildren: './contact-list/contact-list.module#ContactListPageModule' },
      { path: ':id', loadChildren: './contact-list/view-contact/view-contact.module#ViewContactPageModule' },
    ]
  },
  { path: 'add-contact', loadChildren: './contact-list/add-contact/add-contact.module#AddContactPageModule' }
  
  
  
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
