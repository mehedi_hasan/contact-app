import { Component, OnInit } from '@angular/core';
import { ContactService } from '../contact.service';
import { Contact } from '../contact.model';
import { Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.page.html',
  styleUrls: ['./add-contact.page.scss'],
})
export class AddContactPage implements OnInit {
  contact: Contact;
  public name: string;
  public phone: string;
  public email: string;
  public id: number;
  toast: any;
  constructor(
      private contactService: ContactService,
      private router: Router,
      private alertCtrl: AlertController,
      private toastCtrl: ToastController,
    ) { }

  ngOnInit() {
    this.name = '';
    this.phone = '';
    this.email = '';
  }
  newContact() {
    console.log(this.name);
    if (this.name === '') {
      console.log('name empty');
      this.showAlert('Name cannot be empty!');
    } else if (this.phone === '') {
      this.showAlert('Phone Cannot be empty!');
    } else {
      this.id = this.contactService.getLastId() + 1;
      this.contactService.addContact({id: this.id, name: this.name, phone: this.phone, email: this.email});
      this.showToast('Successfull contact added!');
      this.router.navigateByUrl('/contact-list');
    }
  }
  async showAlert(msg: any) {
    const alert = await this.alertCtrl.create({
      message: msg,
      buttons: [
        {
          text: 'ok',
          cssClass: 'icon-color',
          handler: () => {
          }
        },
      ]
    });
    await alert.present();
  }
  showToast(msg: any) {
    this.toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    }).then((toastData) => {
      toastData.present();
    });
  }

}
