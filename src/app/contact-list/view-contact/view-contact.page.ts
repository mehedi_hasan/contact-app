import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactService } from '../contact.service';
import { Contact } from '../contact.model';
import { AlertController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-view-contact',
  templateUrl: './view-contact.page.html',
  styleUrls: ['./view-contact.page.scss'],
})
export class ViewContactPage implements OnInit {
  contact: Contact;
  public id = '';
   toast: any;
   profileImage: any;

  constructor(
      private contactService: ContactService,
      private activatedRoute: ActivatedRoute,
      private router: Router,
      private alertCtrl: AlertController,
      private toastCtrl: ToastController,
    ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.id === '1') {
      this.profileImage = './assets/mehedi.jpg';
    } else {
      this.profileImage = './assets/profile.jpg';
    }
    this.contact = this.contactService.getContact(Number(this.id));
  }

  async deleteContact(id: number) {
    const alert = await this.alertCtrl.create({
      message: 'Do you want to delete this?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'icon-color',
          handler: () => {
            console.log('Cancel clicked');
            this.showToast('Delete Canceled');
          }
        },
        {
          text: 'Ok',
          cssClass: 'icon-color',
          handler: data => {
            this.contactService.deleteContact(id);
            console.log('Items Removed!');
            this.showToast('Successfully Contact Deleted');
            this.router.navigateByUrl('/contact-list');
          }
        }
      ]
    });
    await alert.present();
  }
  showToast( msg: any ) {
    this.toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    }).then((toastData) => {
      toastData.present();
    });
  }
}
