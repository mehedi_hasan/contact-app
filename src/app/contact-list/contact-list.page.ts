import { Component, OnInit } from '@angular/core';
import { ContactService } from './contact.service';
import { Contact } from './contact.model';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.page.html',
  styleUrls: ['./contact-list.page.scss'],
})
export class ContactListPage implements OnInit {
  allContact: Contact[];
  constructor(private contactService: ContactService) { }

  ngOnInit() { }
  ionViewWillEnter() {
    this.allContact = this.contactService.getAllContacts();
    console.log(this.allContact);
  }

}
