import { Injectable } from '@angular/core';
import { Contact } from './contact.model';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  private contacts: Contact[] = [
    {
      id: 1,
      name: 'Mehedi Hasan',
      phone: '01781340027',
      email: 'mmhn.cse@gmail.com'
    }
  ];

  constructor() { }
  addContact(contact: Contact) {
    this.contacts.push(contact);
  }

  totalNumberOfContact() {
    return this.contacts.length;
  }
  getLastId() {
    if (this.contacts === undefined || this.contacts.length === 0) {
      return 0;
    }
    const contact = this.contacts.slice().pop();
    return contact.id;
  }

  getAllContacts() {
    return this.contacts.slice();
  }
  getContact(id: number) {
    return {...this.contacts.find(contact => {
      return contact.id === id;
    })
  };
  }
  deleteContact(id: number) {
    const index = this.contacts.findIndex(contact => contact.id === id);
    return this.contacts.splice(index, 1);
  }
}
